class ProductsController < ApplicationController
layout 'admin'
    def create
      @company = Company.all
      @admin_product = Product.new(create_patams)
       @id = Product.where(id: [@cid])
       if @admin_product.save
           redirect_to :action => 'show', :id => @admin_product.id, :notice => "Product successFully added"
       else
			     render :action => 'new', :alert => "Something Went Wrong"
       end
    end

 
    def index
        @admin_products = Product.where(p_deleted_at: [nil, ""])
    end

    def new
    	@admin_product = Product.new
    	@company = Company.all
    end
 
    def edit
     	@company = Company.all
      @admin_product = Product.find(params[:id])
    end

    def trash
      trash = Product.find(params[:id])
      trash.update_attributes(p_deleted_at: DateTime.now.to_date)
      redirect_to products_trash_products_path
    end

    def product_trash
      @c_trash = Product.where.not(p_deleted_at: [nil, ""])
    end
      
    def restore
      restore = Product.find(params[:id])
      restore.update_attributes(p_deleted_at: nil)
      redirect_to products_trash_products_path
    end 

    def show
        @admin_product = Product.find(params[:id])
    end
 
    def update
       @admin_product = Product.find(params[:id])
       if @admin_product.update_attributes(create_patams)
		    	redirect_to :action => 'show', :id => @admin_product.id
       end 
    end

  def destroy
    @admin_product = Product.find(params[:id])
    @admin_product.destroy
    redirect_to :action => 'index'
  end

    private
        def create_patams
            params.require(:product).permit(:code,:name,:price,:description,:company_id,:image)
             
        end
end
