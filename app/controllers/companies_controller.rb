class CompaniesController < ApplicationController
layout 'admin'
 
    def create
      abort params.inspect
      @admin_company = Company.new(create_patams)
       if @admin_company.save 
           redirect_to :action => 'show', :id => @admin_company.id, :notice => "company successFully added"
       else
           render :action => 'new', :alert => "Something Went Wrong"
        end
    end
 
    def index
        @admin_companies = Company.where(c_deleted_at: [nil, ""])
    end

    def new
      @admin_company = Company.new
    end

    def edit
      @admin_company = Company.find(params[:id])
    end
 
    def show
      @admin_company = Company.find(params[:id])
    end
 
    def update
       @admin_company = Company.find(params[:id])
       if @admin_company.update_attributes(create_patams)
          redirect_to :action => 'show', :id => @admin_company.id
       end 
    end
 

    def trash
    trash = Company.find(params[:id])
    trash.update_attributes(c_deleted_at: DateTime.now.to_date)
    redirect_to companies_trash_companies_path

    end

    def company_trash
      @c_trash = Company.where.not(c_deleted_at: [nil, ""])
    end
      
    def restore
      restore = Company.find(params[:id])
      restore.update_attributes(c_deleted_at: nil)
      redirect_to companies_trash_companies_path
    end  

    def destroy
      @admin_company = Company.find(params[:id])
      @admin_product = Product.where(company_id: params[:id])

      @admin_product.each do |ap|
          ap.destroy
      end  
      @admin_company.destroy
      redirect_to :action => 'index'
    end
        
    private
        def create_patams
            params.require(:company).permit(:name, :address, :phone, :email, :data, :image)
        end

end
