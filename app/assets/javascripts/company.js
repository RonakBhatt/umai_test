// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require admin/pace.min
//= require jquery
//= require jquery_ujs
//= require admin/bootstrap.min
//= require admin/blockui.min
//= require admin/pnotify.min
//= require admin/spectrum
//= require admin/app
//= require admin/picker_color
//= require admin/components_notifications_pnotify
//= require admin/validate.min.js
//= require admin/d3.min
//= require admin/c3.min
//= require admin/datatables.min
//= require admin/select2.min
//= require admin/bootbox.min
//= require admin/sweet_alert.min
//= require admin/validate.min
//= require admin/uniform.min
//= require admin/login_validation
//= require admin/facebook
