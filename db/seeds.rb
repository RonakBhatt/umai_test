# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'ffaker'

5.times do
     User.create(
        first_name: FFaker::NameBR.first_name,
        last_name: FFaker::NameBR.last_name,
        gender: FFaker::Identification.gender,
        phone: FFaker::PhoneNumberAU.mobile_phone_number,
        email: FFaker::Internet.email,
        password: '123456789',
        password_confirmation: '123456789',
      ) 

      Company.create(
		name: FFaker::Company.name,
		address: FFaker::Address.street_address,
		phone: FFaker::PhoneNumber.phone_number,
		email: FFaker::Internet.email,
      	)

		Product.create(
			code: FFaker::NatoAlphabet.code,
			name: FFaker::Company.name,
			price: FFaker::PhoneNumber.area_code,
			description: FFaker::DizzleIpsum.phrase,
			Company_id: 3 + Random.rand(3),
		)
end 
