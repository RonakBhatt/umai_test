class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :code
      t.string :name
      t.float :price
      t.text :description
      t.references :Company, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
