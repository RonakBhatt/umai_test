class AddDeletedAtToProducts < ActiveRecord::Migration
  def change
    add_column :products, :p_deleted_at, :datetime
    add_column :companies, :c_deleted_at, :datetime
  end
end
