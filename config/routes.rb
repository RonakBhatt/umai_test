Rails.application.routes.draw do
  

  get 'dashboards/index'
  devise_for :users
  devise_scope :user do
  root to: "devise/sessions#new"
  end

  resources :products do
	  collection do
	  	get 'trash' => "products#trash", :as => :trash
	  	get 'product_trash' => "products#product_trash", :as => :products_trash
	  	get 'restore' => "products#restore", :as => :restore
	  end
	end

	resources :companies do
	  collection do
	  	get 'trash' => "companies#trash", :as => :trash
	  	get 'company_trash' => "companies#company_trash", :as => :companies_trash
	  	get 'restore' => "companies#restore", :as => :restore
	  end
	end

end
